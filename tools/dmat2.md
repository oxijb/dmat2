src_dir: ./tmp/
output_dir: ../doc/
project_github: https://github.com/drijbush/dmat2
project_website: https://github.com
summary: Reworking of the distributed matrix library
author: Ian Bush
author_description: Short and overweight, but damn fine at chess
github: https://github.com/drijbush
email: Mind your own business
fpp_extensions: fpp
predocmark: >
media_dir: ./media
docmark_alt: #
predocmark_alt: <
display: public
sort: permission-alpha
proc_internals: none
exclude: matrix.f90
         ks_matrix_module.f90
	 replicated_1d_container.f90
	 replicated_2d_container.f90
	 matrix_mapping.f90
	 numbers_dmat2.f90
	 replicated_scalar_container.f90
	 scalapack_interfaces.f90
	 replicated_container.f90
	 blacs_interfaces.f90
	 proc_mapping.f90
source: false
graph: true
search: false
macro: TEST
       LOGIC=.true.
license: by-nc
extra_filetypes:

Hi, my name is ${USER}.
