Module ks_extract_tests
  Use test_params
  Use mpi
  Implicit None
Contains

  Subroutine test_ks_array_remap()

    Use numbers_module , Only : wp
    Use ks_array_module, Only : ks_array, ks_array_init, ks_array_comm_to_base, ks_array_finalise, &
         K_POINT_REAL, K_POINT_COMPLEX
    Use mpi            , Only : mpi_comm_world, mpi_double_complex, mpi_double_precision

    Type( ks_array ) :: A, A_split
    Type( ks_array ) :: B, B_split
    Type( ks_array ) :: C, C_split
    Type( ks_array ) :: D

    Type( ks_array ) :: base_k

    Complex( wp ), Dimension( :, :, :, : ), Allocatable :: A_c
    Complex( wp ), Dimension( :, :    ), Allocatable :: tmp_c

    Real( wp ), Dimension( :, :, :, : ), Allocatable :: A_r
    Real( wp ), Dimension( :, :    ), Allocatable :: tmp_r

    Real( wp ) :: rand
    Real( wp ) :: max_diff

    Integer, Dimension( 1:3, 1:nk ) :: k_points
    Integer, Dimension(      1:nk ) :: k_types

    Integer :: k, s
    Integer :: n
    Integer :: r1, r2, c1, c2
    Integer :: error

    r1 = 2
    r2 = n_block + 1
    c1 = 3
    c2 = n_block + 2

    n = Max( m, r1, r2, c1, c2 )

    Allocate( A_r( 1:n, 1:n, 1:nk, 1:ns ) )
    Allocate( A_c( 1:n, 1:n, 1:nk, 1:ns ) )
    Allocate( tmp_r( 1:n, 1:n ) )
    Allocate( tmp_c( 1:n, 1:n ) )

    A_r = Huge( A_r )
    A_c = Huge( Real( A_c, Kind( A_c ) ) )

    If( me == 0 ) Then

       Call Random_Number( A_r )

       Do s = 1, ns
          Do k = 1, nk
             Call Random_Number( tmp_r )
             A_c( :, :, k, s ) = tmp_r
             Call Random_Number( tmp_r )
             A_c( :, :, k, s ) = A_c( :, :, k, s ) + Cmplx( 0.0_wp, tmp_r, Kind = wp )
          End Do
       End Do

       Do k = 1, nk
          k_points( :, k ) = [ k - 1, 0, 0 ]
          Call Random_Number( rand )
          k_types( k ) = Merge( K_POINT_REAL, K_POINT_COMPLEX, rand > 0.5_wp )
       End Do

    End If

    Call mpi_bcast( k_points, Size( k_points ), mpi_integer, 0, mpi_comm_world, error )
    Call mpi_bcast( k_types , Size( k_types  ), mpi_integer, 0, mpi_comm_world, error )

    Call mpi_bcast( A_r, Size( A_r ), mpi_double_precision, 0, mpi_comm_world, error )

    Call mpi_bcast( A_c, Size( A_c ), mpi_double_complex  , 0, mpi_comm_world, error )

    Call ks_array_init( n_block )
    Call ks_array_comm_to_base( MPI_COMM_WORLD, ns, k_types, k_points, base_k )

    Call A%create( n, n, base_k )
    If( verbose ) Then
       Call A%print_info( 'A', 200 )
    End If
    Do s = 1, ns
       Do k = 1, nk
          If( k_types( k ) == K_POINT_REAL ) Then
             Call A%set_by_global( k_points( :, k ), s, 1, n, 1, n, A_r( :, :, k, s ) )
          Else
             Call A%set_by_global( k_points( :, k ), s, 1, n, 1, n, A_c( :, :, k, s ) )
          End If
       End Do
    End Do

    ! This time actually do some operations in both distrubtions with communications
    ! to reallt check the communicators, contexts etc. have been set up.
    Call A%split_ks( 2.0_wp, A_split )
    If( verbose ) Then
       Call A_split%print_info( 'A_split', 100 )
    End If

    B = A
    If( verbose ) Then
       Call B%print_info( 'B', 100 )
    End If

    Call B%remap( A_split, B_Split, error )
    If( verbose ) Then
       Call B_split%print_info( 'B_split', 100 )
    End If

    C_split = B_split * A_split
    If( verbose ) Then
       Call C_split%print_info( 'C_split', 100 )
    End If

    Call C_split%remap( A, C, error )
    If( verbose ) Then
       Call C%print_info( 'C', 100 )
    End If

    D = A * A - C
    If( verbose ) Then
       Call D%print_info( 'D', 100 )
    End If

    Deallocate( tmp_r )
    Deallocate( tmp_c )
    Allocate( tmp_r( 1:n, 1:n ) )
    Allocate( tmp_c( 1:n, 1:n ) )

    max_diff = -1.0_wp
    Do s = 1, ns
       Do k = 1, nk
          If( k_types( k ) == K_POINT_REAL ) Then
             Call D%get_by_global( k_points( :, k ), s, 1, n, 1, n, tmp_r )
             tmp_r = Abs( tmp_r )
          Else
             Call D%get_by_global( k_points( :, k ), s, 1, n, 1, n, tmp_c )
             tmp_r = Abs( tmp_c )
          End If
          max_diff = Max( max_diff, Maxval( tmp_r ) )
       End Do
    End Do
    If( me == 0 ) Then
       Write( *, error_format ) 'Error in ks_remap ', max_diff, &
            Merge( passed, FAILED, max_diff < tol )
    End If

    Call ks_array_finalise

  End Subroutine test_ks_array_remap

  Subroutine test_ks_array_extract_transpose()

    Use numbers_module , Only : wp
    Use ks_array_module, Only : ks_array, ks_array_init, ks_array_comm_to_base, ks_array_finalise, &
         K_POINT_REAL, K_POINT_COMPLEX
    Use mpi            , Only : mpi_comm_world, mpi_double_complex, mpi_double_precision

    Type( ks_array ) :: A, A_split
    Type( ks_array ) :: B

    Type( ks_array ) :: base_k

    Complex( wp ), Dimension( :, :, :, : ), Allocatable :: A_c
    Complex( wp ), Dimension( :, :    ), Allocatable :: tmp_c

    Real( wp ), Dimension( :, :, :, : ), Allocatable :: A_r
    Real( wp ), Dimension( :, :    ), Allocatable :: tmp_r

    Real( wp ) :: rand
    Real( wp ) :: max_diff

    Integer, Dimension( 1:3, 1:nk ) :: k_points
    Integer, Dimension(      1:nk ) :: k_types

    Integer :: k, s
    Integer :: n
    Integer :: r1, r2, c1, c2
    Integer :: error

    r1 = 1
    r2 = n_block + 1
    c1 = 1
    c2 = n_block + 2

    n = Max( m, r1, r2, c1, c2 )

    Allocate( A_r( 1:n, 1:n, 1:nk, 1:ns ) )
    Allocate( A_c( 1:n, 1:n, 1:nk, 1:ns ) )
    Allocate( tmp_r( 1:n, 1:n ) )
    Allocate( tmp_c( 1:n, 1:n ) )

    A_r = Huge( A_r )
    A_c = Huge( Real( A_c, Kind( A_c ) ) )

    If( me == 0 ) Then

       Call Random_Number( A_r )

       Do s = 1, ns
          Do k = 1, nk
             Call Random_Number( tmp_r )
             A_c( :, :, k, s ) = tmp_r
             Call Random_Number( tmp_r )
             A_c( :, :, k, s ) = A_c( :, :, k, s ) + Cmplx( 0.0_wp, tmp_r, Kind = wp )
          End Do
       End Do

       Do k = 1, nk
          k_points( :, k ) = [ k - 1, 0, 0 ]
          Call Random_Number( rand )
          k_types( k ) = Merge( K_POINT_REAL, K_POINT_COMPLEX, rand > 0.5_wp )
       End Do

    End If

    Call mpi_bcast( k_points, Size( k_points ), mpi_integer, 0, mpi_comm_world, error )
    Call mpi_bcast( k_types , Size( k_types  ), mpi_integer, 0, mpi_comm_world, error )

    Call mpi_bcast( A_r, Size( A_r ), mpi_double_precision, 0, mpi_comm_world, error )

    Call mpi_bcast( A_c, Size( A_c ), mpi_double_complex  , 0, mpi_comm_world, error )

    Call ks_array_init( n_block )
    Call ks_array_comm_to_base( MPI_COMM_WORLD, ns, k_types, k_points, base_k )

    Call A%create( n, n, base_k )
    If( verbose ) Then
       Call A%print_info( 'A', 200 )
    End If
    Do s = 1, ns
       Do k = 1, nk
          If( k_types( k ) == K_POINT_REAL ) Then
             Call A%set_by_global( k_points( :, k ), s, 1, n, 1, n, A_r( :, :, k, s ) )
          Else
             Call A%set_by_global( k_points( :, k ), s, 1, n, 1, n, A_c( :, :, k, s ) )
          End If
       End Do
    End Do

    Call A%split_ks( 2.0_wp, A_split )
    If( verbose ) Then
       Call A_split%print_info( 'A_split', 100 )
    End If

    Deallocate( tmp_r )
    Deallocate( tmp_c )
    Allocate( tmp_r( r1:r2, c1:c2 ) )
    Allocate( tmp_c( r1:r2, c1:c2 ) )

    A_split = .Dagger. A_split
    B = A_split%extract( r1, r2, c1, c2 )
    If( verbose ) Then
       Call B%print_info( 'B', 100 )
    End If

    max_diff = -1.0_wp
    Do s = 1, ns
       Do k = 1, nk
          If( k_types( k ) == K_POINT_REAL ) Then
             Call B%get_by_global( k_points( :, k ), s, 1, r2 - r1 + 1, 1, c2 - c1 + 1, tmp_r )
             tmp_r = Abs( tmp_r - Transpose( A_r( c1:c2, r1:r2, k, s ) ) )
          Else
             Call B%get_by_global( k_points( :, k ), s, 1, r2 - r1 + 1, 1, c2 - c1 + 1, tmp_c )
             tmp_r = Abs( tmp_c - Transpose( Conjg( A_c( c1:c2, r1:r2, k, s ) ) ) )
          End If
          max_diff = Max( max_diff, Maxval( tmp_r ) )
       End Do
    End Do
    If( me == 0 ) Then
       Write( *, error_format ) 'Error in ks_split extract transpose ', max_diff, &
            Merge( passed, FAILED, max_diff < tol )
    End If

    Call ks_array_finalise

  End Subroutine test_ks_array_extract_transpose

  Subroutine test_ks_array_extract()

    Use numbers_module , Only : wp
    Use ks_array_module, Only : ks_array, ks_array_init, ks_array_comm_to_base, ks_array_finalise, &
         K_POINT_REAL, K_POINT_COMPLEX
    Use mpi            , Only : mpi_comm_world, mpi_double_complex, mpi_double_precision

    Type( ks_array ) :: A, A_split
    Type( ks_array ) :: B

    Type( ks_array ) :: base_k

    Complex( wp ), Dimension( :, :, :, : ), Allocatable :: A_c
    Complex( wp ), Dimension( :, :    ), Allocatable :: tmp_c

    Real( wp ), Dimension( :, :, :, : ), Allocatable :: A_r
    Real( wp ), Dimension( :, :    ), Allocatable :: tmp_r

    Real( wp ) :: rand
    Real( wp ) :: max_diff

    Integer, Dimension( 1:3, 1:nk ) :: k_points
    Integer, Dimension(      1:nk ) :: k_types

    Integer :: k, s
    Integer :: n
    Integer :: r1, r2, c1, c2
    Integer :: error

    r1 = 2
    r2 = n_block + 1
    c1 = 3
    c2 = n_block + 2

    n = Max( m, r1, r2, c1, c2 )

    Allocate( A_r( 1:n, 1:n, 1:nk, 1:ns ) )
    Allocate( A_c( 1:n, 1:n, 1:nk, 1:ns ) )
    Allocate( tmp_r( 1:n, 1:n ) )
    Allocate( tmp_c( 1:n, 1:n ) )

    A_r = Huge( A_r )
    A_c = Huge( Real( A_c, Kind( A_c ) ) )

    If( me == 0 ) Then

       Call Random_Number( A_r )

       Do s = 1, ns
          Do k = 1, nk
             Call Random_Number( tmp_r )
             A_c( :, :, k, s ) = tmp_r
             Call Random_Number( tmp_r )
             A_c( :, :, k, s ) = A_c( :, :, k, s ) + Cmplx( 0.0_wp, tmp_r, Kind = wp )
          End Do
       End Do

       Do k = 1, nk
          k_points( :, k ) = [ k - 1, 0, 0 ]
          Call Random_Number( rand )
          k_types( k ) = Merge( K_POINT_REAL, K_POINT_COMPLEX, rand > 0.5_wp )
       End Do

    End If

    Call mpi_bcast( k_points, Size( k_points ), mpi_integer, 0, mpi_comm_world, error )
    Call mpi_bcast( k_types , Size( k_types  ), mpi_integer, 0, mpi_comm_world, error )

    Call mpi_bcast( A_r, Size( A_r ), mpi_double_precision, 0, mpi_comm_world, error )

    Call mpi_bcast( A_c, Size( A_c ), mpi_double_complex  , 0, mpi_comm_world, error )

    Call ks_array_init( n_block )
    Call ks_array_comm_to_base( MPI_COMM_WORLD, ns, k_types, k_points, base_k )

    Call A%create( n, n, base_k )
    If( verbose ) Then
       Call A%print_info( 'A', 200 )
    End If
    Do s = 1, ns
       Do k = 1, nk
          If( k_types( k ) == K_POINT_REAL ) Then
             Call A%set_by_global( k_points( :, k ), s, 1, n, 1, n, A_r( :, :, k, s ) )
          Else
             Call A%set_by_global( k_points( :, k ), s, 1, n, 1, n, A_c( :, :, k, s ) )
          End If
       End Do
    End Do

    Call A%split_ks( 2.0_wp, A_split )
    If( verbose ) Then
       Call A_split%print_info( 'A_split', 100 )
    End If

    Deallocate( tmp_r )
    Deallocate( tmp_c )
    Allocate( tmp_r( r1:r2, c1:c2 ) )
    Allocate( tmp_c( r1:r2, c1:c2 ) )

    B = A_split%extract( r1, r2, c1, c2 )
    If( verbose ) Then
       Call B%print_info( 'B', 100 )
    End If

    max_diff = -1.0_wp
    Do s = 1, ns
       Do k = 1, nk
          If( k_types( k ) == K_POINT_REAL ) Then
             Call B%get_by_global( k_points( :, k ), s, 1, r2 - r1 + 1, 1, c2 - c1 + 1, tmp_r )
             tmp_r = Abs( tmp_r - A_r( r1:r2, c1:c2, k, s ) )
          Else
             Call B%get_by_global( k_points( :, k ), s, 1, r2 - r1 + 1, 1, c2 - c1 + 1, tmp_c )
             tmp_r = Abs( tmp_c - A_c( r1:r2, c1:c2, k, s ) )
          End If
          max_diff = Max( max_diff, Maxval( tmp_r ) )
       End Do
    End Do
    If( me == 0 ) Then
       Write( *, error_format ) 'Error in ks_split extract ', max_diff, &
            Merge( passed, FAILED, max_diff < tol )
    End If

    Call ks_array_finalise

  End Subroutine test_ks_array_extract

  Subroutine test_ks_array_extract_vary()

    ! checks on varying size creates and extracts

    Use numbers_module , Only : wp
    Use ks_array_module, Only : ks_array, ks_array_init, ks_array_comm_to_base, ks_array_finalise, &
         K_POINT_REAL, K_POINT_COMPLEX
    Use mpi            , Only : mpi_comm_world, mpi_double_complex, mpi_double_precision

    Type( ks_array ) :: A, A_split
    Type( ks_array ) :: B, C

    Type( ks_array ) :: base_k

    Type replicated_data
       Real   ( wp ), Dimension( :, : ), Allocatable :: A_r
       Complex( wp ), Dimension( :, : ), Allocatable :: A_c
    End Type replicated_data

    Type( replicated_data ), Dimension( :, : ), Allocatable :: A_rep

    Complex( wp ), Dimension( :, :    ), Allocatable :: tmp_c

    Real( wp ), Dimension( :, :    ), Allocatable :: tmp_r

    Real( wp ) :: rand
    Real( wp ) :: max_diff

    Integer, Dimension( :, :    ), Allocatable :: sizes
    Integer, Dimension( :, :, : ), Allocatable :: shapes

    Integer, Dimension( 1:3, 1:nk ) :: k_points
    Integer, Dimension(      1:nk ) :: k_types

    Integer :: k, s, ks
    Integer :: n
    Integer :: r1, r2, c1, c2
    Integer :: error

    r1 = 2
    r2 = n_block + 1
    c1 = 3
    c2 = n_block + 2

    n = Max( m, r1, r2, c1, c2 )

    Allocate( A_rep( 1:nk, 1:ns ) )
    Allocate( sizes( 1:2, 1:nk * ns ) )
    Allocate( shapes( 1:2, 1:2, 1:nk * ns ) )
    
    ks = 0
    Do s = 1, ns
       Do k = 1, nk
          ks = ks + 1
          
          n = m + s * k * 10
          
          sizes( :, ks ) = n

          shapes( 1, 1, ks ) = 1
          shapes( 1, 2, ks ) = 1
          shapes( 2, 1, ks ) = Min( m, m / 2 + ks * 2 )
          shapes( 2, 2, ks ) = Min( m, m / 3 + ks * 4 )
          
          Allocate( A_rep( k, s )%A_r( 1:n, 1:n ) )           
          Allocate( A_rep( k, s )%A_c( 1:n, 1:n ) ) 
          
       End Do
    End Do

    If( me == 0 ) Then

       ks = 0
       Do s = 1, ns
          Do k = 1, nk
             ks = ks + 1


             Call Random_Number( A_rep( k, s )%A_r )
             
             n = Size( A_rep( k, s )%A_c, Dim = 1 )
             Allocate( tmp_r( 1:n, 1:n ) )
             Call Random_Number( tmp_r )
             A_rep( k, s )%A_c( :, : ) = tmp_r
             Call Random_Number( tmp_r )
             A_rep( k, s )%A_c( :, : ) = A_rep( k, s )%A_c( :, : ) + Cmplx( 0.0_wp, tmp_r, Kind = wp )
             Deallocate( tmp_r )
             
          End Do
       End Do

       Do k = 1, nk
          k_points( :, k ) = [ k - 1, 0, 0 ]
          Call Random_Number( rand )
          k_types( k ) = Merge( K_POINT_REAL, K_POINT_COMPLEX, rand > 0.5_wp )
       End Do

    End If

    Call mpi_bcast( k_points, Size( k_points ), mpi_integer, 0, mpi_comm_world, error )
    Call mpi_bcast( k_types , Size( k_types  ), mpi_integer, 0, mpi_comm_world, error )

    Do s = 1, ns
       Do k = 1, nk
          Call mpi_bcast( A_rep( k, s )%A_r, Size( A_rep( k, s )%A_r ), mpi_double_precision, 0, mpi_comm_world, error )
          Call mpi_bcast( A_rep( k, s )%A_c, Size( A_rep( k, s )%A_c ), mpi_double_complex  , 0, mpi_comm_world, error )
       End Do
    End Do

    Call ks_array_init( n_block )
    Call ks_array_comm_to_base( MPI_COMM_WORLD, ns, k_types, k_points, base_k )

    Call A%create( sizes, base_k )
    If( verbose ) Then
       Call A%print_info( 'A', 200 )
    End If
    ks = 0
    Do s = 1, ns
       Do k = 1, nk
          ks = ks + 1
          If( k_types( k ) == K_POINT_REAL ) Then
             Call A%set_by_global( k_points( :, k ), s, 1, sizes( 1, ks ), 1, sizes( 1, ks ), A_rep( k, s )%A_r )
          Else
             Call A%set_by_global( k_points( :, k ), s, 1, sizes( 1, ks ), 1, sizes( 1, ks ), A_rep( k, s )%A_c )
          End If
       End Do
    End Do

    Call A%split_ks( 2.0_wp, A_split )
    If( verbose ) Then
       Call A_split%print_info( 'A_split', 100 )
    End If


    B = A_split%extract( shapes )
    If( verbose ) Then
       Call B%print_info( 'B', 100 )
    End If

    max_diff = -1.0_wp
    ks = 0
    Do s = 1, ns
       Do k = 1, nk
          ks = ks + 1
          r1 = shapes( 1, 1, ks )
          r2 = shapes( 2, 1, ks )
          c1 = shapes( 1, 2, ks )
          c2 = shapes( 2, 2, ks )
          Allocate( tmp_r( r1:r2, c1:c2 ) )
          Allocate( tmp_c( r1:r2, c1:c2 ) )
          If( k_types( k ) == K_POINT_REAL ) Then
             Call B%get_by_global( k_points( :, k ), s, 1, r2 - r1 + 1, 1, c2 - c1 + 1, tmp_r )
             tmp_r = Abs( tmp_r - A_rep( k, s )%A_r( r1:r2, c1:c2 ) )
          Else
             Call B%get_by_global( k_points( :, k ), s, 1, r2 - r1 + 1, 1, c2 - c1 + 1, tmp_c )
             tmp_r = Abs( tmp_c - A_rep( k, s )%A_c( r1:r2, c1:c2 ) )
          End If
          max_diff = Max( max_diff, Maxval( tmp_r ) )
          Deallocate( tmp_r )
          Deallocate( tmp_c )
       End Do
    End Do
    If( me == 0 ) Then
       Write( *, error_format ) 'Error in ks_split extract vary ', max_diff, &
            Merge( passed, FAILED, max_diff < tol )
    End If

    ! Check multiplies on the varying size arrays work
    C = .Dagger. B
    A = B * C
    If( verbose ) Then
       Call A%print_info( 'A', 100 )
    End If

    max_diff = -1.0_wp
    ks = 0
    Do s = 1, ns
       Do k = 1, nk
          ks = ks + 1
          r1 = shapes( 1, 1, ks )
          r2 = shapes( 2, 1, ks )
          c1 = shapes( 1, 2, ks )
          c2 = shapes( 2, 2, ks )
          Allocate( tmp_r( r1:r2, r1:r2 ) )
          Allocate( tmp_c( r1:r2, r1:r2 ) )
          If( k_types( k ) == K_POINT_REAL ) Then
             Call A%get_by_global( k_points( :, k ), s, 1, r2 - r1 + 1, 1, r2 - r1 + 1, tmp_r )
             tmp_r = Abs( tmp_r - &
                  Matmul( A_rep( k, s )%A_r( r1:r2, c1:c2 ), Transpose( A_rep( k, s )%A_r( r1:r2, c1:c2 ) ) ) )
          Else
             Call A%get_by_global( k_points( :, k ), s, 1, r2 - r1 + 1, 1, r2 - r1 + 1, tmp_c )
             tmp_r = Abs( tmp_c - &
                  Matmul( A_rep( k, s )%A_c( r1:r2, c1:c2 ), Transpose( Conjg( A_rep( k, s )%A_c( r1:r2, c1:c2 ) ) ) ) )
          End If
          max_diff = Max( max_diff, Maxval( tmp_r ) )
          Deallocate( tmp_r )
          Deallocate( tmp_c )
       End Do
    End Do
    If( me == 0 ) Then
       Write( *, error_format ) 'Error in ks_split multiply vary ', max_diff, &
            Merge( passed, FAILED, max_diff < tol )
    End If

    Call ks_array_finalise

  End Subroutine test_ks_array_extract_vary

  Subroutine test_ks_array_get_diagonal()

    Use numbers_module , Only : wp
    Use ks_array_module, Only : ks_array, ks_array_init, ks_array_comm_to_base, ks_array_finalise, &
         K_POINT_REAL, K_POINT_COMPLEX
    Use mpi            , Only : mpi_comm_world, mpi_double_complex, mpi_double_precision

    Type( ks_array ) :: A, A_split

    Type( ks_array ) :: base_k

    Complex( wp ), Dimension( :, :, :, : ), Allocatable :: A_c
    Complex( wp ), Dimension( :, :    ), Allocatable :: tmp_c
    Complex( wp ), Dimension( : ), Allocatable :: diag_c

    Real( wp ), Dimension( :, :, :, : ), Allocatable :: A_r
    Real( wp ), Dimension( :, :    ), Allocatable :: tmp_r
    Real( wp ), Dimension( : ), Allocatable :: diag_r

    Real( wp ) :: rand
    Real( wp ) :: max_diff

    Integer, Dimension( 1:3, 1:nk ) :: k_points
    Integer, Dimension(      1:nk ) :: k_types

    Integer :: k, s
    Integer :: n
    Integer :: r1, r2, c1, c2
    Integer :: error
    Integer :: i

    r1 = 2
    r2 = n_block + 1
    c1 = 3
    c2 = n_block + 2

    n = Max( m, r1, r2, c1, c2 )

    Allocate( A_r( 1:n, 1:n, 1:nk, 1:ns ) )
    Allocate( A_c( 1:n, 1:n, 1:nk, 1:ns ) )
    Allocate( tmp_r( 1:n, 1:n ) )
    Allocate( tmp_c( 1:n, 1:n ) )

    A_r = Huge( A_r )
    A_c = Huge( Real( A_c, Kind( A_c ) ) )

    If( me == 0 ) Then

       Call Random_Number( A_r )

       Do s = 1, ns
          Do k = 1, nk
             Call Random_Number( tmp_r )
             A_c( :, :, k, s ) = tmp_r
             Call Random_Number( tmp_r )
             A_c( :, :, k, s ) = A_c( :, :, k, s ) + Cmplx( 0.0_wp, tmp_r, Kind = wp )
          End Do
       End Do

       Do k = 1, nk
          k_points( :, k ) = [ k - 1, 0, 0 ]
          Call Random_Number( rand )
          k_types( k ) = Merge( K_POINT_REAL, K_POINT_COMPLEX, rand > 0.5_wp )
       End Do

    End If

    Call mpi_bcast( k_points, Size( k_points ), mpi_integer, 0, mpi_comm_world, error )
    Call mpi_bcast( k_types , Size( k_types  ), mpi_integer, 0, mpi_comm_world, error )

    Call mpi_bcast( A_r, Size( A_r ), mpi_double_precision, 0, mpi_comm_world, error )

    Call mpi_bcast( A_c, Size( A_c ), mpi_double_complex  , 0, mpi_comm_world, error )

    Call ks_array_init( n_block )
    Call ks_array_comm_to_base( MPI_COMM_WORLD, ns, k_types, k_points, base_k )

    Call A%create( n, n, base_k )
    If( verbose ) Then
       Call A%print_info( 'A', 200 )
    End If
    Do s = 1, ns
       Do k = 1, nk
          If( k_types( k ) == K_POINT_REAL ) Then
             Call A%set_by_global( k_points( :, k ), s, 1, n, 1, n, A_r( :, :, k, s ) )
          Else
             Call A%set_by_global( k_points( :, k ), s, 1, n, 1, n, A_c( :, :, k, s ) )
          End If
       End Do
    End Do

    Call A%split_ks( 2.0_wp, A_split )
    If( verbose ) Then
       Call A_split%print_info( 'A_split', 100 )
    End If

    Deallocate( tmp_r )
    Deallocate( tmp_c )
    Allocate( diag_r( 1:n ) )
    Allocate( diag_c( 1:n ) )

    max_diff = -1.0_wp
    Do s = 1, ns
       Do k = 1, nk
          If( k_types( k ) == K_POINT_REAL ) Then
             Call A_split%get_diagonal( k_points( :, k ), s, diag_r )
             Do i = 1, n
                diag_r( i ) = Abs( diag_r( i ) - A_r( i, i, k, s ) )
             End Do
          Else
             Call A_split%get_diagonal( k_points( :, k ), s, diag_c )
             Do i = 1, n
                diag_c( i ) = Abs( diag_c( i ) - A_c( i, i, k, s ) )
             End Do
             diag_r = Abs( diag_c )
          End If
          max_diff = Max( max_diff, Maxval( diag_r ) )
       End Do
    End Do
    If( me == 0 ) Then
       Write( *, error_format ) 'Error in ks_split get_diag ', max_diff, &
            Merge( passed, FAILED, max_diff < tol )
    End If

    Call ks_array_finalise

  End Subroutine test_ks_array_get_diagonal

  Subroutine test_ks_array_get_diagonal_replicated()

    Use numbers_module , Only : wp
    Use ks_array_module, Only : ks_array, ks_array_init, ks_array_comm_to_base, ks_array_finalise, &
         K_POINT_REAL, K_POINT_COMPLEX, ks_array_replicated_1d
    Use mpi            , Only : mpi_comm_world, mpi_double_complex, mpi_double_precision

    Type( ks_array ) :: A, A_split

    Type( ks_array ) :: base_k

    Type( ks_array_replicated_1d ), Dimension( : ), Allocatable :: diagonal
    
    Complex( wp ), Dimension( :, :, :, : ), Allocatable :: A_c
    Complex( wp ), Dimension( :, :    ), Allocatable :: tmp_c
    Complex( wp ), Dimension( : ), Allocatable :: diag_c

    Real( wp ), Dimension( :, :, :, : ), Allocatable :: A_r
    Real( wp ), Dimension( :, :    ), Allocatable :: tmp_r
    Real( wp ), Dimension( : ), Allocatable :: diag_r

    Real( wp ) :: rand
    Real( wp ) :: max_diff

    Integer, Dimension( 1:3, 1:nk ) :: k_points
    Integer, Dimension(      1:nk ) :: k_types

    Integer :: k, s
    Integer :: r1, r2, c1, c2
    Integer :: error
    Integer :: i, iks

    r1 = 2
    r2 = n_block + 1
    c1 = 3
    c2 = n_block + 2

    Allocate( A_r( 1:m, 1:n, 1:nk, 1:ns ) )
    Allocate( A_c( 1:m, 1:n, 1:nk, 1:ns ) )
    Allocate( tmp_r( 1:m, 1:n ) )
    Allocate( tmp_c( 1:m, 1:n ) )

    A_r = Huge( A_r )
    A_c = Huge( Real( A_c, Kind( A_c ) ) )

    If( me == 0 ) Then

       Call Random_Number( A_r )

       Do s = 1, ns
          Do k = 1, nk
             Call Random_Number( tmp_r )
             A_c( :, :, k, s ) = tmp_r
             Call Random_Number( tmp_r )
             A_c( :, :, k, s ) = A_c( :, :, k, s ) + Cmplx( 0.0_wp, tmp_r, Kind = wp )
          End Do
       End Do

       Do k = 1, nk
          k_points( :, k ) = [ k - 1, 0, 0 ]
          Call Random_Number( rand )
          k_types( k ) = Merge( K_POINT_REAL, K_POINT_COMPLEX, rand > 0.5_wp )
       End Do

    End If

    Call mpi_bcast( k_points, Size( k_points ), mpi_integer, 0, mpi_comm_world, error )
    Call mpi_bcast( k_types , Size( k_types  ), mpi_integer, 0, mpi_comm_world, error )

    Call mpi_bcast( A_r, Size( A_r ), mpi_double_precision, 0, mpi_comm_world, error )

    Call mpi_bcast( A_c, Size( A_c ), mpi_double_complex  , 0, mpi_comm_world, error )

    Call ks_array_init( n_block )
    Call ks_array_comm_to_base( MPI_COMM_WORLD, ns, k_types, k_points, base_k )

    Call A%create( m, n, base_k )
    If( verbose ) Then
       Call A%print_info( 'A', 200 )
    End If
    Do s = 1, ns
       Do k = 1, nk
          If( k_types( k ) == K_POINT_REAL ) Then
             Call A%set_by_global( k_points( :, k ), s, 1, m, 1, n, A_r( :, :, k, s ) )
          Else
             Call A%set_by_global( k_points( :, k ), s, 1, m, 1, n, A_c( :, :, k, s ) )
          End If
       End Do
    End Do

    Call A%split_ks( 2.0_wp, A_split )
    If( verbose ) Then
       Call A_split%print_info( 'A_split', 100 )
    End If

    Deallocate( tmp_r )
    Deallocate( tmp_c )

    Call A_split%get_diagonal( diagonal )

    max_diff = -1.0_wp
    iks = 0
    Do s = 1, ns
       Do k = 1, nk
          iks = iks + 1
          If( diagonal( iks )%ks_point%k_type == K_POINT_REAL ) Then
             diag_r = diagonal( iks )
             Do i = 1, Min( n, m )
                diag_r( i ) = Abs( diag_r( i ) - A_r( i, i, k, s ) )
             End Do
          Else
             diag_c = diagonal( iks )
             Do i = 1, Min( n, m )
                diag_c( i ) = Abs( diag_c( i ) - A_c( i, i, k, s ) )
             End Do
             diag_r = Abs( diag_c )
          End If
          max_diff = Max( max_diff, Maxval( diag_r ) )
       End Do
    End Do
    If( me == 0 ) Then
       Write( *, error_format ) 'Error in ks_split get_diag_repl ', max_diff, &
            Merge( passed, FAILED, max_diff < tol )
    End If

    Call ks_array_finalise

  End Subroutine test_ks_array_get_diagonal_replicated

  Subroutine test_ks_array_get_column()

    Use numbers_module , Only : wp
    Use ks_array_module, Only : ks_array, ks_array_init, ks_array_comm_to_base, ks_array_finalise, &
         K_POINT_REAL, K_POINT_COMPLEX
    Use mpi            , Only : mpi_comm_world, mpi_double_complex, mpi_double_precision

    Type( ks_array ) :: A, A_split

    Type( ks_array ) :: base_k

    Complex( wp ), Dimension( :, :, :, : ), Allocatable :: A_c
    Complex( wp ), Dimension( :, :    ), Allocatable :: tmp_c
    Complex( wp ), Dimension( : ), Allocatable :: col_c

    Real( wp ), Dimension( :, :, :, : ), Allocatable :: A_r
    Real( wp ), Dimension( :, :    ), Allocatable :: tmp_r
    Real( wp ), Dimension( : ), Allocatable :: col_r

    Real( wp ) :: rand
    Real( wp ) :: max_diff

    Integer, Dimension( 1:3, 1:nk ) :: k_points
    Integer, Dimension(      1:nk ) :: k_types

    Integer :: k, s
    Integer :: r1, r2, c1, c2
    Integer :: error
    Integer :: n_col
    Integer :: i

    r1 = 2
    r2 = n_block + 1
    c1 = 3
    c2 = n_block + 2

    n_col = n / 2 + 1
    
    Allocate( A_r( 1:m, 1:n, 1:nk, 1:ns ) )
    Allocate( A_c( 1:m, 1:n, 1:nk, 1:ns ) )
    Allocate( tmp_r( 1:m, 1:n ) )
    Allocate( tmp_c( 1:m, 1:n ) )

    A_r = Huge( A_r )
    A_c = Huge( Real( A_c, Kind( A_c ) ) )

    If( me == 0 ) Then

       Call Random_Number( A_r )

       Do s = 1, ns
          Do k = 1, nk
             Call Random_Number( tmp_r )
             A_c( :, :, k, s ) = tmp_r
             Call Random_Number( tmp_r )
             A_c( :, :, k, s ) = A_c( :, :, k, s ) + Cmplx( 0.0_wp, tmp_r, Kind = wp )
          End Do
       End Do

       Do k = 1, nk
          k_points( :, k ) = [ k - 1, 0, 0 ]
          Call Random_Number( rand )
          k_types( k ) = Merge( K_POINT_REAL, K_POINT_COMPLEX, rand > 0.5_wp )
       End Do

    End If

    Call mpi_bcast( k_points, Size( k_points ), mpi_integer, 0, mpi_comm_world, error )
    Call mpi_bcast( k_types , Size( k_types  ), mpi_integer, 0, mpi_comm_world, error )

    Call mpi_bcast( A_r, Size( A_r ), mpi_double_precision, 0, mpi_comm_world, error )

    Call mpi_bcast( A_c, Size( A_c ), mpi_double_complex  , 0, mpi_comm_world, error )

    Call ks_array_init( n_block )
    Call ks_array_comm_to_base( MPI_COMM_WORLD, ns, k_types, k_points, base_k )

    Call A%create( m, n, base_k )
    If( verbose ) Then
       Call A%print_info( 'A', 200 )
    End If
    Do s = 1, ns
       Do k = 1, nk
          If( k_types( k ) == K_POINT_REAL ) Then
             Call A%set_by_global( k_points( :, k ), s, 1, m, 1, n, A_r( :, :, k, s ) )
          Else
             Call A%set_by_global( k_points( :, k ), s, 1, m, 1, n, A_c( :, :, k, s ) )
          End If
       End Do
    End Do

    Call A%split_ks( 2.0_wp, A_split )
    If( verbose ) Then
       Call A_split%print_info( 'A_split', 100 )
    End If

    Deallocate( tmp_r )
    Deallocate( tmp_c )
    Allocate( col_r( 1:m ) )
    Allocate( col_c( 1:m ) )

    max_diff = -1.0_wp
    Do s = 1, ns
       Do k = 1, nk
          If( k_types( k ) == K_POINT_REAL ) Then
             Call A_split%get_column( k_points( :, k ), s, n_col, col_r )
             Do i = 1, m
                col_r( i ) = Abs( col_r( i ) - A_r( i, n_col, k, s ) )
             End Do
          Else
             Call A_split%get_column( k_points( :, k ), s, n_col, col_c )
             Do i = 1, m
                col_c( i ) = Abs( col_c( i ) - A_c( i, n_col, k, s ) )
             End Do
             col_r = Abs( col_c )
          End If
          max_diff = Max( max_diff, Maxval( col_r ) )
       End Do
    End Do
    If( me == 0 ) Then
       Write( *, error_format ) 'Error in ks_split get_col ', max_diff, &
            Merge( passed, FAILED, max_diff < tol )
    End If

    Call ks_array_finalise

  End Subroutine test_ks_array_get_column

  Subroutine test_ks_array_get_column_transpose()

    Use numbers_module , Only : wp
    Use ks_array_module, Only : ks_array, ks_array_init, ks_array_comm_to_base, ks_array_finalise, &
         K_POINT_REAL, K_POINT_COMPLEX
    Use mpi            , Only : mpi_comm_world, mpi_double_complex, mpi_double_precision

    Type( ks_array ) :: A, A_split

    Type( ks_array ) :: base_k

    Complex( wp ), Dimension( :, :, :, : ), Allocatable :: A_c
    Complex( wp ), Dimension( :, :    ), Allocatable :: tmp_c
    Complex( wp ), Dimension( : ), Allocatable :: col_c

    Real( wp ), Dimension( :, :, :, : ), Allocatable :: A_r
    Real( wp ), Dimension( :, :    ), Allocatable :: tmp_r
    Real( wp ), Dimension( : ), Allocatable :: col_r

    Real( wp ) :: rand
    Real( wp ) :: max_diff

    Integer, Dimension( 1:3, 1:nk ) :: k_points
    Integer, Dimension(      1:nk ) :: k_types

    Integer :: k, s
    Integer :: r1, r2, c1, c2
    Integer :: error
    Integer :: n_col
    Integer :: i

    r1 = 2
    r2 = n_block + 1
    c1 = 3
    c2 = n_block + 2

    n_col = m / 2 + 1
    
    Allocate( A_r( 1:m, 1:n, 1:nk, 1:ns ) )
    Allocate( A_c( 1:m, 1:n, 1:nk, 1:ns ) )
    Allocate( tmp_r( 1:m, 1:n ) )
    Allocate( tmp_c( 1:m, 1:n ) )

    A_r = Huge( A_r )
    A_c = Huge( Real( A_c, Kind( A_c ) ) )

    If( me == 0 ) Then

       Call Random_Number( A_r )

       Do s = 1, ns
          Do k = 1, nk
             Call Random_Number( tmp_r )
             A_c( :, :, k, s ) = tmp_r
             Call Random_Number( tmp_r )
             A_c( :, :, k, s ) = A_c( :, :, k, s ) + Cmplx( 0.0_wp, tmp_r, Kind = wp )
          End Do
       End Do

       Do k = 1, nk
          k_points( :, k ) = [ k - 1, 0, 0 ]
          Call Random_Number( rand )
          k_types( k ) = Merge( K_POINT_REAL, K_POINT_COMPLEX, rand > 0.5_wp )
       End Do

    End If

    Call mpi_bcast( k_points, Size( k_points ), mpi_integer, 0, mpi_comm_world, error )
    Call mpi_bcast( k_types , Size( k_types  ), mpi_integer, 0, mpi_comm_world, error )

    Call mpi_bcast( A_r, Size( A_r ), mpi_double_precision, 0, mpi_comm_world, error )

    Call mpi_bcast( A_c, Size( A_c ), mpi_double_complex  , 0, mpi_comm_world, error )

    Call ks_array_init( n_block )
    Call ks_array_comm_to_base( MPI_COMM_WORLD, ns, k_types, k_points, base_k )

    Call A%create( m, n, base_k )
    If( verbose ) Then
       Call A%print_info( 'A', 200 )
    End If
    Do s = 1, ns
       Do k = 1, nk
          If( k_types( k ) == K_POINT_REAL ) Then
             Call A%set_by_global( k_points( :, k ), s, 1, m, 1, n, A_r( :, :, k, s ) )
          Else
             Call A%set_by_global( k_points( :, k ), s, 1, m, 1, n, A_c( :, :, k, s ) )
          End If
       End Do
    End Do

    Call A%split_ks( 2.0_wp, A_split )
    If( verbose ) Then
       Call A_split%print_info( 'A_split', 100 )
    End If
    A_split = .Dagger. A_split
       
    Deallocate( tmp_r )
    Deallocate( tmp_c )
    Allocate( col_r( 1:n ) )
    Allocate( col_c( 1:n ) )

    max_diff = -1.0_wp
    Do s = 1, ns
       Do k = 1, nk
          If( k_types( k ) == K_POINT_REAL ) Then
             Call A_split%get_column( k_points( :, k ), s, n_col, col_r )
             Do i = 1, n
                col_r( i ) = Abs( col_r( i ) - A_r( n_col, i, k, s ) )
             End Do
          Else
             Call A_split%get_column( k_points( :, k ), s, n_col, col_c )
             Do i = 1, n
                col_c( i ) = Abs( col_c( i ) - Conjg( A_c( n_col, i, k, s ) ) )
             End Do
             col_r = Abs( col_c )
          End If
          max_diff = Max( max_diff, Maxval( col_r ) )
       End Do
    End Do
    If( me == 0 ) Then
       Write( *, error_format ) 'Error in ks_split get_col (T) ', max_diff, &
            Merge( passed, FAILED, max_diff < tol )
    End If

    Call ks_array_finalise

  End Subroutine test_ks_array_get_column_transpose

  Subroutine test_ks_array_get_column_replicated()

    Use numbers_module , Only : wp
    Use ks_array_module, Only : ks_array, ks_array_init, ks_array_comm_to_base, ks_array_finalise, &
         K_POINT_REAL, K_POINT_COMPLEX, ks_array_replicated_1d
    Use mpi            , Only : mpi_comm_world, mpi_double_complex, mpi_double_precision

    Type( ks_array ) :: A, A_split

    Type( ks_array ) :: base_k

    Type( ks_array_replicated_1d ), Dimension( : ), Allocatable :: column
    
    Complex( wp ), Dimension( :, :, :, : ), Allocatable :: A_c
    Complex( wp ), Dimension( :, :    ), Allocatable :: tmp_c
    Complex( wp ), Dimension( : ), Allocatable :: col_c

    Real( wp ), Dimension( :, :, :, : ), Allocatable :: A_r
    Real( wp ), Dimension( :, :    ), Allocatable :: tmp_r
    Real( wp ), Dimension( : ), Allocatable :: col_r

    Real( wp ) :: rand
    Real( wp ) :: max_diff

    Integer, Dimension( 1:3, 1:nk ) :: k_points
    Integer, Dimension(      1:nk ) :: k_types

    Integer :: k, s
    Integer :: r1, r2, c1, c2
    Integer :: n_col
    Integer :: error
    Integer :: i, iks

    r1 = 2
    r2 = n_block + 1
    c1 = 3
    c2 = n_block + 2

    n_col = n / 2 + 1

    Allocate( A_r( 1:m, 1:n, 1:nk, 1:ns ) )
    Allocate( A_c( 1:m, 1:n, 1:nk, 1:ns ) )
    Allocate( tmp_r( 1:m, 1:n ) )
    Allocate( tmp_c( 1:m, 1:n ) )

    A_r = Huge( A_r )
    A_c = Huge( Real( A_c, Kind( A_c ) ) )

    If( me == 0 ) Then

       Call Random_Number( A_r )

       Do s = 1, ns
          Do k = 1, nk
             Call Random_Number( tmp_r )
             A_c( :, :, k, s ) = tmp_r
             Call Random_Number( tmp_r )
             A_c( :, :, k, s ) = A_c( :, :, k, s ) + Cmplx( 0.0_wp, tmp_r, Kind = wp )
          End Do
       End Do

       Do k = 1, nk
          k_points( :, k ) = [ k - 1, 0, 0 ]
          Call Random_Number( rand )
          k_types( k ) = Merge( K_POINT_REAL, K_POINT_COMPLEX, rand > 0.5_wp )
       End Do

    End If

    Call mpi_bcast( k_points, Size( k_points ), mpi_integer, 0, mpi_comm_world, error )
    Call mpi_bcast( k_types , Size( k_types  ), mpi_integer, 0, mpi_comm_world, error )

    Call mpi_bcast( A_r, Size( A_r ), mpi_double_precision, 0, mpi_comm_world, error )

    Call mpi_bcast( A_c, Size( A_c ), mpi_double_complex  , 0, mpi_comm_world, error )

    Call ks_array_init( n_block )
    Call ks_array_comm_to_base( MPI_COMM_WORLD, ns, k_types, k_points, base_k )

    Call A%create( m, n, base_k )
    If( verbose ) Then
       Call A%print_info( 'A', 200 )
    End If
    Do s = 1, ns
       Do k = 1, nk
          If( k_types( k ) == K_POINT_REAL ) Then
             Call A%set_by_global( k_points( :, k ), s, 1, m, 1, n, A_r( :, :, k, s ) )
          Else
             Call A%set_by_global( k_points( :, k ), s, 1, m, 1, n, A_c( :, :, k, s ) )
          End If
       End Do
    End Do

    Call A%split_ks( 2.0_wp, A_split )
    If( verbose ) Then
       Call A_split%print_info( 'A_split', 100 )
    End If

    Deallocate( tmp_r )
    Deallocate( tmp_c )

    Call A_split%get_column( n_col, column )

    max_diff = -1.0_wp
    iks = 0
    Do s = 1, ns
       Do k = 1, nk
          iks = iks + 1
          If( column( iks )%ks_point%k_type == K_POINT_REAL ) Then
             col_r = column( iks )
             Do i = 1, m
                col_r( i ) = Abs( col_r( i ) - A_r( i, n_col, k, s ) )
             End Do
          Else
             col_c = column( iks )
             Do i = 1, m
                col_c( i ) = Abs( col_c( i ) - A_c( i, n_col, k, s ) )
             End Do
             col_r = Abs( col_c )
          End If
          max_diff = Max( max_diff, Maxval( col_r ) )
       End Do
    End Do
    If( me == 0 ) Then
       Write( *, error_format ) 'Error in ks_split get_col_repl ', max_diff, &
            Merge( passed, FAILED, max_diff < tol )
    End If

    Call ks_array_finalise

  End Subroutine test_ks_array_get_column_replicated

  Subroutine test_ks_array_get_row()

    Use numbers_module , Only : wp
    Use ks_array_module, Only : ks_array, ks_array_init, ks_array_comm_to_base, ks_array_finalise, &
         K_POINT_REAL, K_POINT_COMPLEX
    Use mpi            , Only : mpi_comm_world, mpi_double_complex, mpi_double_precision

    Type( ks_array ) :: A, A_split

    Type( ks_array ) :: base_k

    Complex( wp ), Dimension( :, :, :, : ), Allocatable :: A_c
    Complex( wp ), Dimension( :, :    ), Allocatable :: tmp_c
    Complex( wp ), Dimension( : ), Allocatable :: row_c

    Real( wp ), Dimension( :, :, :, : ), Allocatable :: A_r
    Real( wp ), Dimension( :, :    ), Allocatable :: tmp_r
    Real( wp ), Dimension( : ), Allocatable :: row_r

    Real( wp ) :: rand
    Real( wp ) :: max_diff

    Integer, Dimension( 1:3, 1:nk ) :: k_points
    Integer, Dimension(      1:nk ) :: k_types

    Integer :: k, s
    Integer :: r1, r2, c1, c2
    Integer :: error
    Integer :: m_row
    Integer :: i

    r1 = 2
    r2 = n_block + 1
    c1 = 3
    c2 = n_block + 2

    m_row = m / 2 + 1
    
    Allocate( A_r( 1:m, 1:n, 1:nk, 1:ns ) )
    Allocate( A_c( 1:m, 1:n, 1:nk, 1:ns ) )
    Allocate( tmp_r( 1:m, 1:n ) )
    Allocate( tmp_c( 1:m, 1:n ) )

    A_r = Huge( A_r )
    A_c = Huge( Real( A_c, Kind( A_c ) ) )

    If( me == 0 ) Then

       Call Random_Number( A_r )

       Do s = 1, ns
          Do k = 1, nk
             Call Random_Number( tmp_r )
             A_c( :, :, k, s ) = tmp_r
             Call Random_Number( tmp_r )
             A_c( :, :, k, s ) = A_c( :, :, k, s ) + Cmplx( 0.0_wp, tmp_r, Kind = wp )
          End Do
       End Do

       Do k = 1, nk
          k_points( :, k ) = [ k - 1, 0, 0 ]
          Call Random_Number( rand )
          k_types( k ) = Merge( K_POINT_REAL, K_POINT_COMPLEX, rand > 0.5_wp )
       End Do

    End If

    Call mpi_bcast( k_points, Size( k_points ), mpi_integer, 0, mpi_comm_world, error )
    Call mpi_bcast( k_types , Size( k_types  ), mpi_integer, 0, mpi_comm_world, error )

    Call mpi_bcast( A_r, Size( A_r ), mpi_double_precision, 0, mpi_comm_world, error )

    Call mpi_bcast( A_c, Size( A_c ), mpi_double_complex  , 0, mpi_comm_world, error )

    Call ks_array_init( n_block )
    Call ks_array_comm_to_base( MPI_COMM_WORLD, ns, k_types, k_points, base_k )

    Call A%create( m, n, base_k )
    If( verbose ) Then
       Call A%print_info( 'A', 200 )
    End If
    Do s = 1, ns
       Do k = 1, nk
          If( k_types( k ) == K_POINT_REAL ) Then
             Call A%set_by_global( k_points( :, k ), s, 1, m, 1, n, A_r( :, :, k, s ) )
          Else
             Call A%set_by_global( k_points( :, k ), s, 1, m, 1, n, A_c( :, :, k, s ) )
          End If
       End Do
    End Do

    Call A%split_ks( 2.0_wp, A_split )
    If( verbose ) Then
       Call A_split%print_info( 'A_split', 100 )
    End If

    Deallocate( tmp_r )
    Deallocate( tmp_c )
    Allocate( row_r( 1:n ) )
    Allocate( row_c( 1:n ) )

    max_diff = -1.0_wp
    Do s = 1, ns
       Do k = 1, nk
          If( k_types( k ) == K_POINT_REAL ) Then
             Call A_split%get_row( k_points( :, k ), s, m_row, row_r )
             Do i = 1, n
                row_r( i ) = Abs( row_r( i ) - A_r( m_row, i, k, s ) )
             End Do
          Else
             Call A_split%get_row( k_points( :, k ), s, m_row, row_c )
             Do i = 1, n
                row_c( i ) = Abs( row_c( i ) - A_c( m_row, i, k, s ) )
             End Do
             row_r = Abs( row_c )
          End If
          max_diff = Max( max_diff, Maxval( row_r ) )
       End Do
    End Do
    If( me == 0 ) Then
       Write( *, error_format ) 'Error in ks_split get_row ', max_diff, &
            Merge( passed, FAILED, max_diff < tol )
    End If

    Call ks_array_finalise

  End Subroutine test_ks_array_get_row

  Subroutine test_ks_array_get_row_transpose()

    Use numbers_module , Only : wp
    Use ks_array_module, Only : ks_array, ks_array_init, ks_array_comm_to_base, ks_array_finalise, &
         K_POINT_REAL, K_POINT_COMPLEX
    Use mpi            , Only : mpi_comm_world, mpi_double_complex, mpi_double_precision

    Type( ks_array ) :: A, A_split

    Type( ks_array ) :: base_k

    Complex( wp ), Dimension( :, :, :, : ), Allocatable :: A_c
    Complex( wp ), Dimension( :, :    ), Allocatable :: tmp_c
    Complex( wp ), Dimension( : ), Allocatable :: row_c

    Real( wp ), Dimension( :, :, :, : ), Allocatable :: A_r
    Real( wp ), Dimension( :, :    ), Allocatable :: tmp_r
    Real( wp ), Dimension( : ), Allocatable :: row_r

    Real( wp ) :: rand
    Real( wp ) :: max_diff

    Integer, Dimension( 1:3, 1:nk ) :: k_points
    Integer, Dimension(      1:nk ) :: k_types

    Integer :: k, s
    Integer :: r1, r2, c1, c2
    Integer :: error
    Integer :: m_row
    Integer :: i

    r1 = 2
    r2 = n_block + 1
    c1 = 3
    c2 = n_block + 2

    m_row = n / 2 + 1
    
    Allocate( A_r( 1:m, 1:n, 1:nk, 1:ns ) )
    Allocate( A_c( 1:m, 1:n, 1:nk, 1:ns ) )
    Allocate( tmp_r( 1:m, 1:n ) )
    Allocate( tmp_c( 1:m, 1:n ) )

    A_r = Huge( A_r )
    A_c = Huge( Real( A_c, Kind( A_c ) ) )

    If( me == 0 ) Then

       Call Random_Number( A_r )

       Do s = 1, ns
          Do k = 1, nk
             Call Random_Number( tmp_r )
             A_c( :, :, k, s ) = tmp_r
             Call Random_Number( tmp_r )
             A_c( :, :, k, s ) = A_c( :, :, k, s ) + Cmplx( 0.0_wp, tmp_r, Kind = wp )
          End Do
       End Do

       Do k = 1, nk
          k_points( :, k ) = [ k - 1, 0, 0 ]
          Call Random_Number( rand )
          k_types( k ) = Merge( K_POINT_REAL, K_POINT_COMPLEX, rand > 0.5_wp )
       End Do

    End If

    Call mpi_bcast( k_points, Size( k_points ), mpi_integer, 0, mpi_comm_world, error )
    Call mpi_bcast( k_types , Size( k_types  ), mpi_integer, 0, mpi_comm_world, error )

    Call mpi_bcast( A_r, Size( A_r ), mpi_double_precision, 0, mpi_comm_world, error )

    Call mpi_bcast( A_c, Size( A_c ), mpi_double_complex  , 0, mpi_comm_world, error )

    Call ks_array_init( n_block )
    Call ks_array_comm_to_base( MPI_COMM_WORLD, ns, k_types, k_points, base_k )

    Call A%create( m, n, base_k )
    If( verbose ) Then
       Call A%print_info( 'A', 200 )
    End If
    Do s = 1, ns
       Do k = 1, nk
          If( k_types( k ) == K_POINT_REAL ) Then
             Call A%set_by_global( k_points( :, k ), s, 1, m, 1, n, A_r( :, :, k, s ) )
          Else
             Call A%set_by_global( k_points( :, k ), s, 1, m, 1, n, A_c( :, :, k, s ) )
          End If
       End Do
    End Do

    Call A%split_ks( 2.0_wp, A_split )
    If( verbose ) Then
       Call A_split%print_info( 'A_split', 100 )
    End If
    A_split = .Dagger. A_split
       
    Deallocate( tmp_r )
    Deallocate( tmp_c )
    Allocate( row_r( 1:m ) )
    Allocate( row_c( 1:m ) )

    max_diff = -1.0_wp
    Do s = 1, ns
       Do k = 1, nk
          If( k_types( k ) == K_POINT_REAL ) Then
             Call A_split%get_row( k_points( :, k ), s, m_row, row_r )
             Do i = 1, m
                row_r( i ) = Abs( row_r( i ) - A_r( i, m_row, k, s ) )
             End Do
          Else
             Call A_split%get_row( k_points( :, k ), s, m_row, row_c )
             Do i = 1, m
                row_c( i ) = Abs( row_c( i ) - Conjg( A_c( i, m_row, k, s ) ) )
             End Do
             row_r = Abs( row_c )
          End If
          max_diff = Max( max_diff, Maxval( row_r ) )
       End Do
    End Do
    If( me == 0 ) Then
       Write( *, error_format ) 'Error in ks_split get_row (T) ', max_diff, &
            Merge( passed, FAILED, max_diff < tol )
    End If

    Call ks_array_finalise

  End Subroutine test_ks_array_get_row_transpose

  Subroutine test_ks_array_get_row_replicated()

    Use numbers_module , Only : wp
    Use ks_array_module, Only : ks_array, ks_array_init, ks_array_comm_to_base, ks_array_finalise, &
         K_POINT_REAL, K_POINT_COMPLEX, ks_array_replicated_1d
    Use mpi            , Only : mpi_comm_world, mpi_double_complex, mpi_double_precision

    Type( ks_array ) :: A, A_split

    Type( ks_array ) :: base_k

    Type( ks_array_replicated_1d ), Dimension( : ), Allocatable :: row
    
    Complex( wp ), Dimension( :, :, :, : ), Allocatable :: A_c
    Complex( wp ), Dimension( :, :    ), Allocatable :: tmp_c
    Complex( wp ), Dimension( : ), Allocatable :: row_c

    Real( wp ), Dimension( :, :, :, : ), Allocatable :: A_r
    Real( wp ), Dimension( :, :    ), Allocatable :: tmp_r
    Real( wp ), Dimension( : ), Allocatable :: row_r

    Real( wp ) :: rand
    Real( wp ) :: max_diff

    Integer, Dimension( 1:3, 1:nk ) :: k_points
    Integer, Dimension(      1:nk ) :: k_types

    Integer :: k, s
    Integer :: r1, r2, c1, c2
    Integer :: m_row
    Integer :: error
    Integer :: i, iks

    r1 = 2
    r2 = n_block + 1
    c1 = 3
    c2 = n_block + 2

    m_row = m / 2 + 1

    Allocate( A_r( 1:m, 1:n, 1:nk, 1:ns ) )
    Allocate( A_c( 1:m, 1:n, 1:nk, 1:ns ) )
    Allocate( tmp_r( 1:m, 1:n ) )
    Allocate( tmp_c( 1:m, 1:n ) )

    A_r = Huge( A_r )
    A_c = Huge( Real( A_c, Kind( A_c ) ) )

    If( me == 0 ) Then

       Call Random_Number( A_r )

       Do s = 1, ns
          Do k = 1, nk
             Call Random_Number( tmp_r )
             A_c( :, :, k, s ) = tmp_r
             Call Random_Number( tmp_r )
             A_c( :, :, k, s ) = A_c( :, :, k, s ) + Cmplx( 0.0_wp, tmp_r, Kind = wp )
          End Do
       End Do

       Do k = 1, nk
          k_points( :, k ) = [ k - 1, 0, 0 ]
          Call Random_Number( rand )
          k_types( k ) = Merge( K_POINT_REAL, K_POINT_COMPLEX, rand > 0.5_wp )
       End Do

    End If

    Call mpi_bcast( k_points, Size( k_points ), mpi_integer, 0, mpi_comm_world, error )
    Call mpi_bcast( k_types , Size( k_types  ), mpi_integer, 0, mpi_comm_world, error )

    Call mpi_bcast( A_r, Size( A_r ), mpi_double_precision, 0, mpi_comm_world, error )

    Call mpi_bcast( A_c, Size( A_c ), mpi_double_complex  , 0, mpi_comm_world, error )

    Call ks_array_init( n_block )
    Call ks_array_comm_to_base( MPI_COMM_WORLD, ns, k_types, k_points, base_k )

    Call A%create( m, n, base_k )
    If( verbose ) Then
       Call A%print_info( 'A', 200 )
    End If
    Do s = 1, ns
       Do k = 1, nk
          If( k_types( k ) == K_POINT_REAL ) Then
             Call A%set_by_global( k_points( :, k ), s, 1, m, 1, n, A_r( :, :, k, s ) )
          Else
             Call A%set_by_global( k_points( :, k ), s, 1, m, 1, n, A_c( :, :, k, s ) )
          End If
       End Do
    End Do

    Call A%split_ks( 2.0_wp, A_split )
    If( verbose ) Then
       Call A_split%print_info( 'A_split', 100 )
    End If

    Deallocate( tmp_r )
    Deallocate( tmp_c )

    Call A_split%get_row( m_row, row )

    max_diff = -1.0_wp
    iks = 0
    Do s = 1, ns
       Do k = 1, nk
          iks = iks + 1
          If( row( iks )%ks_point%k_type == K_POINT_REAL ) Then
             row_r = row( iks )
             Do i = 1, n
                row_r( i ) = Abs( row_r( i ) - A_r( m_row, i, k, s ) )
             End Do
          Else
             row_c = row( iks )
             Do i = 1, n
                row_c( i ) = Abs( row_c( i ) - A_c( m_row, i, k, s ) )
             End Do
             row_r = Abs( row_c )
          End If
          max_diff = Max( max_diff, Maxval( row_r ) )
       End Do
    End Do
    If( me == 0 ) Then
       Write( *, error_format ) 'Error in ks_split get_row_repl ', max_diff, &
            Merge( passed, FAILED, max_diff < tol )
    End If

    Call ks_array_finalise

  End Subroutine test_ks_array_get_row_replicated

  Subroutine test_ks_array_get_patch_replicated()

    Use numbers_module , Only : wp
    Use ks_array_module, Only : ks_array, ks_array_init, ks_array_comm_to_base, ks_array_finalise, &
         K_POINT_REAL, K_POINT_COMPLEX, ks_array_replicated_2d
    Use mpi            , Only : mpi_comm_world, mpi_double_complex, mpi_double_precision

    Type( ks_array ) :: A, A_split

    Type( ks_array ) :: base_k

    Type( ks_array_replicated_2d ), Dimension( : ), Allocatable :: Data
    
    Complex( wp ), Dimension( :, :, :, : ), Allocatable :: A_c
    Complex( wp ), Dimension( :, :    ), Allocatable :: tmp_c
    Complex( wp ), Dimension( :, : ), Allocatable :: data_c

    Real( wp ), Dimension( :, :, :, : ), Allocatable :: A_r
    Real( wp ), Dimension( :, :    ), Allocatable :: tmp_r
    Real( wp ), Dimension( :, : ), Allocatable :: data_r

    Real( wp ) :: rand
    Real( wp ) :: max_diff

    Integer, Dimension( 1:3, 1:nk ) :: k_points
    Integer, Dimension(      1:nk ) :: k_types

    Integer :: k, s
    Integer :: m1, m2, n1, n2
    Integer :: error
    Integer :: i, j, iks

    m1 = m / 3 + 1
    m2 = m / 2 + 1
    n1 = n / 4 + 1
    n2 = n / 2 + 1
    
    Allocate( A_r( 1:m, 1:n, 1:nk, 1:ns ) )
    Allocate( A_c( 1:m, 1:n, 1:nk, 1:ns ) )
    Allocate( tmp_r( 1:m, 1:n ) )
    Allocate( tmp_c( 1:m, 1:n ) )

    A_r = Huge( A_r )
    A_c = Huge( Real( A_c, Kind( A_c ) ) )

    If( me == 0 ) Then

       Call Random_Number( A_r )

       Do s = 1, ns
          Do k = 1, nk
             Call Random_Number( tmp_r )
             A_c( :, :, k, s ) = tmp_r
             Call Random_Number( tmp_r )
             A_c( :, :, k, s ) = A_c( :, :, k, s ) + Cmplx( 0.0_wp, tmp_r, Kind = wp )
          End Do
       End Do

       Do k = 1, nk
          k_points( :, k ) = [ k - 1, 0, 0 ]
          Call Random_Number( rand )
          k_types( k ) = Merge( K_POINT_REAL, K_POINT_COMPLEX, rand > 0.5_wp )
       End Do

    End If

    Call mpi_bcast( k_points, Size( k_points ), mpi_integer, 0, mpi_comm_world, error )
    Call mpi_bcast( k_types , Size( k_types  ), mpi_integer, 0, mpi_comm_world, error )

    Call mpi_bcast( A_r, Size( A_r ), mpi_double_precision, 0, mpi_comm_world, error )

    Call mpi_bcast( A_c, Size( A_c ), mpi_double_complex  , 0, mpi_comm_world, error )

    Call ks_array_init( n_block )
    Call ks_array_comm_to_base( MPI_COMM_WORLD, ns, k_types, k_points, base_k )

    Call A%create( m, n, base_k )
    If( verbose ) Then
       Call A%print_info( 'A', 200 )
    End If
    Do s = 1, ns
       Do k = 1, nk
          If( k_types( k ) == K_POINT_REAL ) Then
             Call A%set_by_global( k_points( :, k ), s, 1, m, 1, n, A_r( :, :, k, s ) )
          Else
             Call A%set_by_global( k_points( :, k ), s, 1, m, 1, n, A_c( :, :, k, s ) )
          End If
       End Do
    End Do

    Call A%split_ks( 2.0_wp, A_split )
    If( verbose ) Then
       Call A_split%print_info( 'A_split', 100 )
    End If

    Deallocate( tmp_r )
    Deallocate( tmp_c )

    Call A_split%get_by_global( m1, m2, n1, n2, Data )

    max_diff = -1.0_wp
    iks = 0
    Do s = 1, ns
       Do k = 1, nk
          iks = iks + 1
          If( Data( iks )%ks_point%k_type == K_POINT_REAL ) Then
             data_r = Data( iks )
             Do i = m1, m2
                Do j = n1, n2
                   data_r( i - m1 + 1, j - n1 + 1 ) = Abs( data_r( i - m1 + 1, j - n1 + 1 ) - A_r( i, j, k, s ) )
                End Do
             End Do
          Else
             data_c = Data( iks )
             Do i = m1, m2
                Do j = n1, n2
                   data_c( i - m1 + 1, j - n1 + 1 ) = Abs( data_c( i - m1 + 1, j - n1 + 1 ) - A_c( i, j, k, s ) )
                End Do
             End Do
             data_r = Abs( data_c )
          End If
          max_diff = Max( max_diff, Maxval( data_r ) )
       End Do
    End Do
    If( me == 0 ) Then
       Write( *, error_format ) 'Error in ks_split get_glob_repl ', max_diff, &
            Merge( passed, FAILED, max_diff < tol )
    End If

    Call ks_array_finalise

  End Subroutine test_ks_array_get_patch_replicated

End Module ks_extract_tests

